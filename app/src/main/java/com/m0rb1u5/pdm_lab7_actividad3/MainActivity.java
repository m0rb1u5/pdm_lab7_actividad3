package com.m0rb1u5.pdm_lab7_actividad3;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
   private EditText editText1, editText2, editText3, editText4;
   private Cursor fila;

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_main);
      editText1 = (EditText) findViewById(R.id.editText);
      editText2 = (EditText) findViewById(R.id.editText2);
      editText3 = (EditText) findViewById(R.id.editText3);
      editText4 = (EditText) findViewById(R.id.editText4);
   }
   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      // Inflate the menu; this adds items to the action bar if it is present.
      getMenuInflater().inflate(R.menu.menu_main, menu);
      return true;
   }
   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      // Handle action bar item clicks here. The action bar will
      // automatically handle clicks on the Home/Up button, so long
      // as you specify a parent activity in AndroidManifest.xml.
      int id = item.getItemId();

      //noinspection SimplifiableIfStatement
      if (id == R.id.action_settings) {
         return true;
      }

      return super.onOptionsItemSelected(item);
   }

   public void alta(View view) {
      AdminSQLite adminSQLite =
            new AdminSQLite(this, "administracion", null, 1);
      SQLiteDatabase database = adminSQLite.getWritableDatabase();
      String dni = editText1.getText().toString();
      String nombre = editText2.getText().toString();
      String colegio = editText3.getText().toString();
      String nromesa = editText4.getText().toString();
      Cursor fila = database.rawQuery("select * from votantes where dni=" +
         dni, null);
      if (!fila.moveToFirst()) {
         ContentValues registro = new ContentValues();
         registro.put("dni", dni);
         registro.put("nombre", nombre);
         registro.put("colegio", colegio);
         registro.put("nromesa", nromesa);
         database.insert("votantes", null, registro);
         database.close();
         editText1.setText("");
         editText2.setText("");
         editText3.setText("");
         editText4.setText("");
         Toast.makeText(this, "Datos actualizados", Toast.LENGTH_SHORT).show();
      }
      else {
         database.close();
         Toast.makeText(this, "Contacto existente", Toast.LENGTH_SHORT).show();
      }
   }
   public void baja(View view) {
      AdminSQLite adminSQLite =
            new AdminSQLite(this, "administracion", null, 1);
      SQLiteDatabase database = adminSQLite.getWritableDatabase();
      String dni = editText1.getText().toString();
      int cant = database.delete("votantes", "dni=" + dni, null);
      database.close();
      editText1.setText("");
      editText2.setText("");
      editText3.setText("");
      editText4.setText("");
      if (cant==1) Toast.makeText(this, "Borrado", Toast.LENGTH_SHORT).show();
      else Toast.makeText(this, "No existe", Toast.LENGTH_SHORT).show();
   }
   public void cons(View view) {
      AdminSQLite adminSQLite =
            new AdminSQLite(this, "administracion", null, 1);
      SQLiteDatabase database = adminSQLite.getWritableDatabase();
      String dni = editText1.getText().toString();
      Cursor fila = database.rawQuery(
            "select nombre, colegio, nromesa from votantes where dni=" +
                  dni, null);
      if (fila.moveToFirst()) {
         editText2.setText(fila.getString(0));
         editText3.setText(fila.getString(1));
         editText4.setText(fila.getString(2));
      }
      else Toast.makeText(this, "No existe persona", Toast.LENGTH_SHORT).show();
      database.close();
   }
   public void modif(View view) {
      AdminSQLite adminSQLite =
            new AdminSQLite(this, "administracion", null, 1);
      SQLiteDatabase database = adminSQLite.getWritableDatabase();
      String dni = editText1.getText().toString();
      String nombre = editText2.getText().toString();
      String colegio = editText3.getText().toString();
      String nromesa = editText4.getText().toString();
      ContentValues registro = new ContentValues();
      registro.put("nombre", nombre);
      registro.put("colegio", colegio);
      registro.put("nromesa", nromesa);
      int cant = database.update("votantes", registro, "dni=" + dni, null);
      database.close();
      if (cant==1) Toast.makeText(this, "Modificación realizada", Toast.LENGTH_SHORT).show();
      else Toast.makeText(this, "No se encuentra", Toast.LENGTH_SHORT).show();
   }
   public void inici(View view) {
      AdminSQLite adminSQLite =
            new AdminSQLite(this, "administracion", null, 1);
      SQLiteDatabase database = adminSQLite.getWritableDatabase();
      fila = database.rawQuery("select * from votantes order by dni asc ", null);
      if (fila.moveToFirst()) {
         editText1.setText(fila.getString(0));
         editText2.setText(fila.getString(1));
         editText3.setText(fila.getString(2));
         editText4.setText(fila.getString(3));
      }
      else Toast.makeText(this, "No hay registrados", Toast.LENGTH_SHORT).show();
      database.close();
   }
   public void ant(View view) {
      try {
         if(!fila.isFirst()) {
            fila.moveToPrevious();
            editText1.setText(fila.getString(0));
            editText2.setText(fila.getString(1));
            editText3.setText(fila.getString(2));
            editText4.setText(fila.getString(3));
         }
         else Toast.makeText(this, "Inicio de la tabla", Toast.LENGTH_SHORT).show();
      }
      catch (Exception error) {
         error.printStackTrace();
      }
   }
   public void sig(View view) {
      try {
         if(!fila.isLast()) {
            fila.moveToNext();
            editText1.setText(fila.getString(0));
            editText2.setText(fila.getString(1));
            editText3.setText(fila.getString(2));
            editText4.setText(fila.getString(3));
         }
         else Toast.makeText(this, "Llegó al final", Toast.LENGTH_SHORT).show();
      }
      catch (Exception error) {
         error.printStackTrace();
      }
   }
   public void fin(View view) {
      AdminSQLite adminSQLite =
            new AdminSQLite(this, "administracion", null, 1);
      SQLiteDatabase database = adminSQLite.getWritableDatabase();
      fila = database.rawQuery("select * from votantes order by dni asc ", null);
      if (fila.moveToLast()) {
         editText1.setText(fila.getString(0));
         editText2.setText(fila.getString(1));
         editText3.setText(fila.getString(2));
         editText4.setText(fila.getString(3));
      }
      else Toast.makeText(this, "No hay registros", Toast.LENGTH_SHORT).show();
      database.close();
   }
   public void resetInfo(View view) {
      editText1.setText("");
      editText2.setText("");
      editText3.setText("");
      editText4.setText("");
   }
}
